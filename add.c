#include "Node.h"
	
int get_node(struct node*);
	
int add(struct node* node,int number){
	
	int current=0, flag=0, j=0, first=0;
	
	int nextn=get_node(node);
        
	if (nextn == MYNULL) return(0);
	
	node[nextn].data=number;
	
	if (node[0].next == MYNULL) flag=1; //checks to see if its the first node on the list
	
	if (number < node[node[0].next].data) first=1; //is checking to see if it needs to be put before the first input
	
	while (j <= 99 && flag == 0 && first == 0){
		current=node[current].next;
		if (node[current].next == MYNULL) flag=2;
		if (number < node[node[current].next].data) flag=3;
		//printf("DEBUG j:%i flag:%i\n", j, flag);
		j++;
	}
		
	if (flag != 0 || first != 0){
		//printf("DEBUG current:%i nextn:%i flag:%i j:%i\n", current, nextn, flag, j);
		node[nextn].next=node[current].next;
		node[current].next=nextn;
		return(1);
	}
	
	return(0);
}

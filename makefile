PA1: PA1.o delete.o add.o get_node.o init.o print.o search.o release_node.o Node.h
	gcc -o PA1 PA1.o delete.o add.o get_node.o init.o print.o search.o release_node.o

PA1.o : PA1.c delete.c add.c init.c print.c search.c Node.h
	gcc -c PA1.c delete.c add.c init.c print.c search.c

delete.o: delete.c release_node.c Node.h 
	gcc -c delete.c Node.h 

add.o: add.c get_node.c Node.h
	gcc -c add.c get_node.c Node.h

print.o: print.c Node.h
	gcc -c print.c

search.o: search.c Node.h
	gcc -c search.c

init.o: init.c Node.h
	gcc -c init.c

clean:
	rm PA1 PA1.o delete.o add.o print.o release_node.o get_node.o search.o init.o

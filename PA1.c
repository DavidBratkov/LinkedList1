// This is Programing assignment 1 by David Bratkov
//
// CSE 222
//
// 1/22/2020
//
// This program basically is building a linked list that the user can manipulate with given commands
// 

#include "Node.h"

void init(struct node*); // ODP 202
int add(struct node*,int); // works with all cases
void print(struct node*); // works with all cases except when the list is empty
int delete(struct node*,int); // Reworked to work with the nodes at the end
int search(struct node*,int); // ODP 204?

int main() {
struct node LL[100];
void init(struct node* LL);

printf("Hello and welcome to David Bratkov's PA1\nHere you will need to type in a few commands\nEach command will help you manipulate a list of data\n");
printf("The format should be as follows:i 5*enter* to insert 5\n");

init(LL);

//This is a debug function that will check if it works when it's full
/*
LL[0].next=1;
for (int j=1; j != 98; j++){
LL[j].data=j;
LL[j].next=(j+1);
LL[j].valid=1;
}
LL[98].data=201;
LL[98].next=-1;
LL[98].valid=1;
*/

char command=0;
char temp[120], temp1[120];
int number, rvalue, flag;

while (command != 'x'){ // This is the main loop 
command=0;
number=0;
flag=0;
while (flag == 0){ // this is the loop for inputs
printf(">");
fgets(temp, 120, stdin);
rvalue=sscanf(temp, "%c%i%s", &command, &number, temp1);//the main sscanf function
//printf("DEBUG command:%c number:%i\n",command, number);
if (command == 'x') return(0);
if (command != 'i' && command != 'p' && command != 's' && command != 'd' && command != 'x') printf("The commands are i(insert), p(print), s(search), d(delete), x(exit)\n");
if (command == 'i' || command == 'p' || command == 's' || command == 'd' || command == 'x') flag=1;

if (flag == 1){//this function is to check whenever there is a number after the command
int test=0;
if (command == 'i') test=sscanf(temp,"i %i", &number);
if (command == 's') test=sscanf(temp,"s %i", &number);
if (command == 'd') test=sscanf(temp,"d %i", &number);
//printf("DEBUG test:%i command:%c\n", test, command);
if (test == -1) printf("Please input a number after the command\n");
if (test == -1) flag=0;
//if (command == 'p' || command == 'x' || command == 'z') flag=1;

}


}

//printf("DEBUG It left the loop!\n");

if (command == 'p'){ // print command
if (LL[0].next != MYNULL) print(LL);
if (LL[0].next == MYNULL) printf("The List is empty!\n"); // there is no need to print if its empty
}

int error=-1, temp2=0; // two temp variables one to see if the function returns with an error and the other is for the search function
if (command == 'i'){ // insert command
if (LL[0].next != MYNULL) temp2=search(LL,number);// searches to see if it's already in the list 
if (temp2 == 0){ // it wont try to add if the search function found the same number
error=add(LL,number);
if(error == 0) printf("Out of Space!\n");
if(error == 1) printf("Success!\n");
}
if (temp2 == 1) printf("Number already in list\n");
}


if (command == 's'){ //search command
temp2=2;
if (LL[0].next == MYNULL) printf("Not found!\n");
if (LL[0].next != MYNULL) temp2=search(LL,number);
if (temp2 == 0) printf("Not Found\n");
if (temp2 == 1) printf("Found\n");
}

int delret=2; // temp variable for the delete function
if (command == 'd'){
temp2=2;
if (LL[0].next == MYNULL) printf("Node not found!\n");
if (LL[0].next != MYNULL) temp2=search(LL,number); //searches before hand to find if it has the same node in the list
if (temp2 == 0) printf("Node not found!\n");
if (temp2 == 1){
delret=delete(LL,number);
if (delret == 1) printf("Success!\n");
//if (delret == 0) printf("Error this shouldnt happen\n"); 
}
}

/*
if (command == 'z'){ // DEBUG tool for seeing the linked list
printf("You selected the DEBUG fuction\n");
int i=0;
printf("The Linked list is:\n");
while (LL[i].next != -99999 && i != 100){
printf("%.3i | %.3i | %.2i | %i |\n", i, LL[i].data, LL[i].next, LL[i].valid);
i++;
}
} */

}//closing bracket of the while != x

}

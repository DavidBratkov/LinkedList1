#include "Node.h"

void release_node(struct node*,int);

int delete(struct node* LL, int number){

int current=0, last;
if (LL[current].next == MYNULL) return(0);

while (LL[current].next != MYNULL && LL[current].data != number){
if (LL[LL[current].next].data == number) last=current;
current=LL[current].next;
}

if (number == LL[current].data){
release_node(LL, current);
LL[last].next=LL[current].next;
return(1);
}

return(0);
}

struct node {
int data;
int next;
int valid;
};

int getFirst(struct node *LL, int *error){
int next=LL[0].next;
if ( next == -1 ) {
*error=1;
return(0);
}
*error=0;
return(LL[next].data);
}
